# BigQuery Integration

Google offers BigQuery, a "petabyte scale" distributed database solution, which
offers schema modeling and a SQL dialect similar (but not compatible) with other
database engines. Like many Google services, they offer access through a clever
HTTP API and Python libraries that model the HTTP architecture. That's not
always the most intuitive, not really supporting the object model in Python, so
naturally some flexibility is needed, with a proper Python abstraction. 
Here we are.

This document is organized starting with the most common use-cases, and ending
with the more obscure, esoteric cases.

## Authorization 

Similar to the [Google Analytics integration](docs/googleanalytics.md), this
interface requires authorization, and for the purposes of this project's focus
on AppEngine, we'll rely on the standard OAuth for Web model.

The BigQuery service can be accessed like so:

```python
from webapptitude import bigquery
from webapptitude import oauthkit

auth = oauthkit.web()  # OAuth credentials factory.
bqsvc = bigquery.BigQueryService(auth)  # the BigQuery proxy.
```

*NOTE*: Code examples in this document will refer to `bqsvc` as declared here.

## Listing Assets

This library abstracts the object hierarchy of BigQuery into their respective tiers:

- The service is authorized to one or more Projects
- Each Project may have multiple Datasets
- Each Dataset may have multiple Tables

These tiers can be interrogated as below:

```python
from webapptitude import bigquery
# bqsvc as defined above

for proj_id, proj_name, project in bqsvc.projects:  # a generator
    assert isinstance(project, bigquery.Project)  # passes
    for dataset_id, dataset_name, dataset in project.datasets:  # a generator
        assert isinstance(dataset, bigquery.Dataset)  # passes
        for table_id, table_name, table in dataset.tables: # a generator
            assert isinstance(table, bigquery.Table)  # passes
            print "Table (project=%r, dataset=%r, table=%r)" % \
                (project_id, dataset_name, table_id)
    
```

## Retrieving Specific Assets by ID

The following methods have also been implemented to enable retrieval of a 
specific asset: 

- `BigQueryService.get_project({project_id})`
- `Project.get_dataset({dataset_id})`
- `Dataset.get_table({table_id})`

Note that these are _instance methods_, not class methods.

## Issuing Queries

Most of the time you'll want to issue a query against a specific Project.

```python
from webapptitude import bigquery

# Fetch the first project. You may want to select it by ID; this functionality 
# is soon forthcoming.
project_id, project = list(bqsvc.projects)[0]
assert isinstance(project, bigquery.Project)

query_text = "SELECT * FROM [dataset.table] WHERE xyz=1"

results = project.query(query_text)  # synchronous is OK for smaller datasets/etc

print repr(results)  # a list of dictionaries
```

Query results are modeled by the library as a series of dictionaries. Each 
dictionary represents a row, its elements named per the columns of the resultset.

### Asynchronous Queries

Larger datasets or more complex queries may require asynchronous operations. 

```python
from webapptitude import bigquery
import time

# Get your project from the `bqsvc` instance...
assert isinstance(project, bigquery.Project)

job = project.query(query_text, async=True)
assert isinstance(job, bigquery.Job)

while True:
    # wait for results.
    try:
        time.sleep(5)
        results = list(job.results)
        break
    except bigquery.JobNotReady:
        continue

print repr(results)  # a list of dictionaries

```

For use in Google AppEngine, near-term upcoming enhancements will leverage the 
Queue and `deferred` features to enable background processing of asynchronous 
queries.

## Managing Datasets

Datasets are, in effect, just groups of tables and views. The `Dataset` class
provides a simple approach to these essential assets.


### Creating Tables
This component aims to simplify management of tables through a very declarative
style.

```python
from webapptitude import bigquery

# Get your dataset from a project...
assert isinstance(dataset, bigquery.Dataset)

table_id = "table_id_123"  # allows alphanumerics and underscores
table_name = "My friendly table name"  # free form
table_instance = dataset.prepare_table(table_id, table_name, dict(
    # {name of field}=({field type}, {field mode}, {field description})
    timestamp=('TIMESTAMP', 'REQUIRED', 'The creation time of this record'),
    counter=('INTEGER', None, 'Some counting value'), # defaults to NULLABLE
    variant=('STRING', None, 'Which variant to count...')
))

assert isinstance(table_instance, bigquery.Table)
```

This `prepare_table` method will create the table if missing, or patch the table
schema if present.

Field types can be any one of:

- `STRING`
- `BYTES`
- `INTEGER`
- `FLOAT`
- `BOOLEAN`
- `TIMESTAMP` 
- `RECORD` (denoting a nested schema)

Field modes can be any one of:

- `REQUIRED`
- `REPEATABLE`
- `NULLABLE` (default)


### Creating Views

BigQuery allows views to be defined in (their dialect of) SQL. 

Each view can incorporate several user-defined functions (UDFs). A UDF can be 
provided in-line, or retrieved from a Google Cloud Storage bucket. The example
below outlines the use of either approach.

See also: [Google's UDF documentation](https://cloud.google.com/bigquery/user-defined-functions)

```python
from webapptitude import bigquery

# Get your dataset from a project...
assert isinstance(dataset, bigquery.Dataset)

view_id = "amazing_view"  # allows alphanumerics and underscores
view_name = "My friendly view name"  # free form
view_instance = dataset.prepare_table(view_id, view_name, 
    "SELECT COUNT(*) FROM [dataset_id.table_id_123];",
    legacy_sql=True,  # using "standard SQL" is currently not recommended in production.
    functions=[
        # This interface allows two models for providing user-defined functions
        # Type 1: The UDF is defined in a script in google cloud storage.
        "gs://bucket/path/udf_asset.js", 

        # Type 2: The JavaScript UDF is defined inline.  See also:
        # https://cloud.google.com/bigquery/user-defined-functions
        """
        function my_udf(row, emit){ // a map/reduce style.
            // each call is given a row.
            emit ({len: (row.variant && row.variant.length)})
        }

        bigquery.defineFunction(
            // how the method will be called in SQL
            'fn_name_in_sql',             

            // which columns this UDF operates on from eacn input record
            ['columns', 'from_record'],   

            // output record schema
            [{name: 'len', type: 'integer'}] ,

            my_udf  // lastly, the function itself.
        )
        """
    ]
)

assert isinstance(view_instance, bigquery.Table)
```


## Inserting Records into Tables

As in the example above, this example will assume a schema like:

- `timestamp` is a required `TIMESTAMP` field
- `counter` is an optional `INTEGER` field
- `variant` is an optional `STRING` field

Most fields (as noted above) convert plainly to native Python types. The one
special case is timestamps, which are encoded in a format similar to ISO-8601.
This library converts between this string format and Python's native `datetime`
model. Further, for timestamp fields, a value of `NOW` represents the current
UTC timestamp (per Python's system time). Eventually this may be translated into
BigQuery SQL representation instead.

```python
from webapptitude import bigquery
from datetime import datetime, timedelta

# Get your table from a project dataset...
assert isinstance(table, bigquery.Table)

tomorrow = datetime.now() + timedelta(days=1)

table.insert(  # multiple records simultaneously.
    dict(timestamp="NOW", counter=None, variant=None),
    dict(timestamp=tomorrow, counter=7, variant="tomorrow")
)

```

## Project-level Queries

The `BigQueryService` object also provides three functions useful for advanced
querying capabilities. These are used internally with the `Project.query(...)`
functionality.

- `BigQueryService.query({project_id}, {sql})` for synchronous queries; returns `list([ dict...])`.
- `BigQueryService.query_async({project_id}, {sql}, batch=True, large=False)` for asynchronous queries; returns a `{job_id}`.
- `BigQueryService.get_results({project_id}, {job_id})` generates results as a series of rows, as dictionaries; this handles pagination through multi-page query results automatically.
