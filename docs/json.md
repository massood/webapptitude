# JSON API Tooling

webapptitude provides a `jsonapi.JSONRequestHandler` to simplify enforcement of 
API contracts. It ensures that requests are of content-type `application/json`, and
that the responses are in kind. 

This handler also provides a method, `fetch_param`, which can enforce validation of
given parameters. This function raises exceptions `ParameterNotFound` or `ParameterMismatch` when missing or when validation fails, respectively.

Example

```python
from webapptitude import jsonapi
from webapptitude import jsonkit
from webapptitude import WSGIApplication

app = WSGIApplication()

@app.route('/api/json')
class RequestHandler(jsonapi.JSONRequestHandler):

    def get(self):

        validator = lambda s: (len(s) == 12)

        self.response.write_json({
            'somevalue': (self.fetch_param('value',  # must be 12-char long.
                                          validate=validator, 
                                          optional=False))
        })

```


## JSON Model API

Many applications benefit from offering an API directly to clients to access
certain models of the data store, especially when front-end code (i.e. Javascript) handles business logic.

This assumes a REST API approach, wherein all requests are handled as JSON:

- `GET` requests to collections perform a search and return a list of matching records
- `GET` requests to specific entities (by key) return the representation of the record.
- `PUT` performs an update to an existing entity (by key)
- `POST` to a collection creates a new entity of that type
- `DELETE` removes a record from a collection (by key)
- `HEAD` returns 200 OK when a record exists, or 404.
- `HEAD` to a collection returns 200, and a header `X-Records-Match` noting the count of matching records; this query supports search parameters.

Notable limitations (currently):

- `DELETE` to a collection is unsupported, even with query parameters.
- `PUT` and `POST` have specific uses in this approach: update and create, respectively.
- `PUT` requires an entity identifier.
- `POST` requires a collection, and fails with an entity identifier.

The class `jsonapi.ModelAPI` defines all relevant HTTP handling methods. It also provides two important hooks, which applications can/should override:

- method `prepare_query` constructs an `ndb.Query` instance, and can be extended to apply additional filters, or constrain the resulting properties (through a projection).
- method `is_authorized` should validate the HTTP method against the specific model, and possibly a specific instance, to enforce access controls.
- method `present` prepares a dictionary for JSON modeling; this can be intercepted to filter specific properties from reaching the client.


Example follows.

```python
from webapptitude import jsonapi
from webapptitude import WSGIApplication
from webapptitude import models

app = WSGIApplication()

class Profile(models.Model):  # a model for the datastore
    name = models.ndb.StringProperty(required=True)

# Attach the model to the application; simplest model, no constraints.
# This sets up routes for this model, by default:
#    - /api/data/i/<model name>/
#    - /api/data/i/<model name>/<key identifier>
# These routes would have model name "Profile" (case-sensitive).
jsonapi.api(Profile).register_routes(app)

# Prepare a model wrapper with constraints...
class NoDeleteModel(jsonapi.api(Profile, "profileNoDelete")):
    
    def is_authorized(self, method, model, instance=None):
        # This would block all DELETE requests.
        return (method not in ('delete', 'DELETE'))

    def prepare_query(self):
        result = super(NoDeleteModel, self).prepare_query()
        # This is an NDB query. See also:
        # https://cloud.google.com/appengine/docs/python/ndb/queries
        return result.filter(self.model.name == 'valid_name')

# Attach the constrained model handler to the app.
# The "profileNoDelete" would override the route path, providing routes:
#    - /nodelete/api/data/i/profileNoDelete/
#    - /nodelete/api/data/i/profileNoDelete/<key identifier>
NoDeleteModel.register_routes(app, prefix='/nodelete')

```

