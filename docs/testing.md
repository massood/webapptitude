# Testing Your Application

webapptitude provides some (hopefully) helpful shortcuts for automating
tests in your application.

This document assumes:

- Your testing process can work well with Python's `unittest` model.
- You keep your test automation code in a directory `test`

## Setting up Tests

A simple application unit-test with webapptitude will generally look about like
so:

```python
from webapptitude import testkit

class MyTestCase(testkit.TestCase):
  
  def testSomeBehavior(self):
    result = performApplicationLogic()
    self.assertTrue(result)

```

When testing through application APIs, webapptitude provides some
foundational classes and boilerplate setup to facilitate a simple wrapper:

```python
from webapptitude import WSGIApplication
from webapptitude import testkit

from myapp import DataAPIRequestHandler  # should respond with JSON

app = WSGIApplication(debug=True)
app.route('/data', DataAPIRequestHandler)

class TestCase(testkit.ApplicationTestCase(app)):

  def testQueryAPI(self):
    response = self.testapp.get('/data/test')
    data = response.json
    self.assertGreater(len(request.body), 0)
    self.assertEqual(data.get('test'), True)

```

### Test Facilities

webapptitude extends AppEngine's Testbed with some sensible shortcuts and
boilerplate setup.

- Provides a testbed instance as `TestCase.testbed`.
- Initializes all stubs (e.g. datastore, ndb, memcache, etc) and handles
teardown (clearing cache, etc).
- Provides a `TestCase.webservice` to launch the application in a separate
thread.

**Note** that to take proper advantage of these, it's important that any
`TestCase` subclasses also call the super-class methods `setUp`,
`tearDown`, `setUpClass`, and `tearDownClass` in any related subclass
methods.

### Simulate a Logged-In User

```python
class TestCase(testkit.TestCase):

  def testSomethingLoggedIn(self):
    self.testbed.loginUser("email@address.com", 12345, is_admin=True)
    # continue test

  def testSomethingAnonymous(self):
    self.testbed.loginAnonymous()
    # continue test

```

### Threading the Webservice

In some (rare) cases it may be necessary to spin up an application in a
separate thread. webapptitude facilitates this using a `contextmanager` to
activate and then terminate the separate thread.

```python
class TestCase(ApplicationTestCase(myWSGIapplication)):

  def testSomething(self):
    with self.webservice as http:
      result = requests.get(http + '/uri_path')
      self.assertEqual(result.status_code, 200)

```

## Command-line Execution

Once you've written tests, you'll want to run them. Google AppEngine
provides some [reasonable testing facilities][1] already, however
webapptitude aims to further simplify it. 

webapptitude provides the boilerplate for:
  
  - Importing dependencies from the `google.appengine` module
  - Measuring test coverage


```shell
source ./virtualenv/bin/activate && \
  pip install -q --upgrade -r requirements.txt && \
  pip install -q --upgrade -r requirements_test.txt && \
  python -m webapptitude.test --coverage -f -vvvv -t . ./test/
```


[1]: https://cloud.google.com/appengine/docs/python/tools/localunittesting
