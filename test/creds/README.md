# SECRETS

These are security credentials. Treat them with great care.
Nothing personal should be stored here.

This project's integration tests require access to Google Analytics and Google BigQuery APIs,
which require authorization to a service account and some sensible (seed) data to support
querying.
